const fetch = require('../../utils/fetch.js')
Page({
  data:{
    slides: [],
    cats:[],
  },
  onLoad(){
    // 1。抓取轮播图界面  
    fetch('./slides').then((res,rej)=>{
      this.setData({
        slides:res.data
      })
    })

    // 2. 抓取九宫格界面
    fetch('./categories').then((res,rej)=>{
      this.setData({
        cats:res.data
      })
    })
    
  }
})
// 
