// pages/list/list.js
const fetch = require('../../utils/fetch.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    categories:[],
    pageIndex:0,
    pageSize: 20,
    shops:[],
    reachBottomFlag: true,
    cat:1,
    searchText:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  loadmore(){
    let params = { _page: ++this.data.pageIndex, _limit: this.data.pageSize }
    if (this.data.searchText) params.q = this.data.searchText
    console.log(params)
    return fetch(`/categories/${this.data.cat}/shops`, params).then(res => {
      const totalCount = parseInt(res.header['X-Total-Count'])
      const reachBottomFlag = totalCount > this.data.pageIndex * this.data.pageSize
      // totalCount = 42   1*10
      const shops = this.data.shops.concat(res.data)
      this.setData({
        shops,
        pageIndex: this.data.pageIndex,
        reachBottomFlag,
        searchText:''
      })
    })
  },
  searchHandle(e){
    this.setData({ shops: [], pageIndex: 0, reachBottomFlag: true })
    this.loadmore()
  },
  inputchangHandle(e){
    this.setData({
      searchText:e.detail.value
    })
  },
  onLoad: function (options) {
    this.setData({
      cat:options.cat
    })
    fetch(`/categories/${this.data.cat}`).then(res=>{
      wx.setNavigationBarTitle({
        title: res.data.name
      })
    })
    this.loadmore()
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.loadmore()
  },
})