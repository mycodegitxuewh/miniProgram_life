const app = getApp()
module.exports = (url,data,method = 'GET',header={})=>{
  return new Promise((resolve,reject)=>{
    wx.request({
      url: app.config.apiBase + url,
      data,
      method,
      header,
      dataType:'json',
      success: resolve,
      fail: reject
    })
  })
}
// Promise(function())
//  